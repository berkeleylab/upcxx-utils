#pragma once

/*
 * File:   thread_pool.hpp
 * Author: regan
 *
 */

#include <functional>
#include <memory>
#include <upcxx/upcxx.hpp>

#include "upcxx_utils/log.hpp"
#include "upcxx_utils/timers.hpp"

// define this to disable all extra thread workers and any calls to enqueue_* will execute immediately
// #define UPCXX_UTILS_NO_THREAD_POOL

namespace upcxx_utils {

#define THREAD_FRIENDLY_POLL_NS 10

class ThreadPool_detail;
class ThreadPool {
  //
  // ThreadPool is based mostly on the ThreadPool.h from https://github.com/progschj/ThreadPool
  // by Jakob Progsch, Václav Zeman.
  //
  // This class was rewritten in 2020 by Rob Egan for use within upcxx and upcxx_utils
  //

  //
  //  Copyright (c) 2012 Jakob Progsch, Václav Zeman
  //
  // This software is provided 'as-is', without any express or implied
  // warranty. In no event will the authors be held liable for any damages
  // arising from the use of this software.
  //
  // Permission is granted to anyone to use this software for any purpose,
  // including commercial applications, and to alter it and redistribute it
  // freely, subject to the following restrictions:
  //
  // 1. The origin of this software must not be misrepresented; you must not
  //    claim that you wrote the original software. If you use this software
  //    in a product, an acknowledgment in the product documentation would be
  //    appreciated but is not required.
  //
  // 2. Altered source versions must be plainly marked as such, and must not be
  //    misrepresented as being the original software.
  //
  // 3. This notice may not be removed or altered from any source
  //    distribution.

 public:
  using Task = std::function<void()>;  // void(void) function/lambda wrappers
  using ShTask = std::shared_ptr<Task>;

  // a thread friendly non-busy wait that alternates calling yield and sleep_ns
  // between calls to upcxx::progress() to ensure ThreadPool tasks get cpu time
  template <typename... T>
  static auto wait(upcxx::future<T...> &fut, uint64_t poll_ns = THREAD_FRIENDLY_POLL_NS) {
    assert(upcxx::master_persona().active_with_caller() && "Called from master persona");
    assert(!upcxx::in_progress() && "Not called within the restricted context");
    BaseTimer t;
    t.start();
    const int progress_to_yield_ratio = 20;
    DBG("Entering ThreadPool::wait\n");
    size_t ct = 0;
    bool is_ready = false;
    while (!(is_ready = fut.is_ready())) {
      auto ct2 = 0;
      do {
        progress();
        if ((is_ready = fut.is_ready())) break;
        if (!upcxx::progress_required()) yield();
        progress();
        if ((is_ready = fut.is_ready())) break;
      } while (upcxx::progress_required() || ct2++ < progress_to_yield_ratio);
      if (is_ready) break;
      if (!upcxx::progress_required()) sleep_ns(poll_ns);
      ct++;
    }
    assert(fut.is_ready());
    t.stop();
    DBG("Completed ThreadPool::wait ct=", ct, " ", t.get_elapsed(), " s\n");
    return fut.wait();  // should be noop
  };

  static void barrier(const upcxx::team &tm = upcxx::world());

  static string get_thread_label();

 private:
  std::unique_ptr<ThreadPool_detail> tp_detail;
  upcxx::future<> _serial_fut;
  upcxx::future<> _pending_fut;

  // is_ready() returns true when a thread needs to wake ( stop condition or a task is enqueued)
  // bool is_ready() const;
  // is_terminal() returns true when a thread needs to stop (stop condition and no tasks are enqueued)
  // bool is_terminal() const;
  // returns true if there are threads in this ThreadPool (spawned or available to be spawned)
  // bool is_active() const;

  void enqueue_task(ShTask sh_task);

  // wrap a function
  template <class Func>
  auto wrap_func(Func &&func) {
    using return_t = typename std::invoke_result<Func>::type;
    using prom_no_ret_t = upcxx::promise<>;
    using prom_ret_t = upcxx::promise<return_t>;

    auto start_t = Timer::now();
    auto task_id = global_task_id()++;

    // only 1 promise and 1 function will actually be used!
    std::shared_ptr<prom_no_ret_t> sh_prom_no_ret{};
    std::shared_ptr<prom_ret_t> sh_prom_ret{};
    
    std::string func_label = make_string("task_id=", task_id, " this=", (void *)this, " sh_prom=");
    if constexpr (std::is_void<return_t>::value) {
      // copy and consume the func (and its lambda captures)
      sh_prom_no_ret = std::make_shared<prom_no_ret_t>();
      func_label += make_string(sh_prom_no_ret.get(), " without returned value");
    } else {
      // copy and consume the func (and its lambda captures)
      sh_prom_ret = std::make_shared<prom_ret_t>();
      func_label += make_string(sh_prom_ret.get(), " with returned value");
    }
    DBG(func_label, "\n");

    ShTask sh_task = std::make_shared<Task>([=, func=std::forward<Func>(func), &persona = upcxx::current_persona()]() {
      DBG_VERBOSE("Executing ", func_label, "\n");
      std::shared_ptr<return_t> sh_ret_value{};

      if constexpr (std::is_void<return_t>::value) {
        assert(sh_prom_no_ret);
        func();
      } else {
        assert(sh_prom_ret);
        sh_ret_value = make_shared<return_t>(std::move(func()));
        //sh_prom_ret->fulfill_result(func());
      }
      DBG_VERBOSE("Finished ", func_label, "\n");

      // fulfill only in the calling persona
      persona.lpc_ff([=, &persona]() {
        assert(upcxx::initialized() && persona.active_with_caller());
        duration_seconds s = Timer::now() - start_t;
        DBG("Fulfilling ", func_label, "\n");
        global_tasks_completed()++;
        if constexpr (std::is_void<return_t>::value) {
          assert(sh_prom_no_ret);
          sh_prom_no_ret->fulfill_anonymous(1);
        } else {
          assert(sh_prom_ret);
          sh_prom_ret->fulfill_result(std::move(*sh_ret_value));
        }
        DBG("Fulfilled ", func_label, " in ", s.count(), " s\n");
      });
    });

    if constexpr (std::is_void<return_t>::value) {
      assert(sh_prom_no_ret);
      // return a future with all lambda captures and owned shared_ptrs still in scope
      auto fut = sh_prom_no_ret->get_future().then(
          [sh_prom_no_ret, sh_task, func_label, func=std::forward<Func>(func)]() { 
            // ensure all destrutors are called after func completes and from the same persona
            DBG("Returned ", func_label, "\n"); });
      _pending_fut = when_all(_pending_fut, fut);
      return make_pair(sh_task, fut);
    } else {
      assert(sh_prom_ret);
      // return a future with all lambda captures and owned shared_ptrs still in scope
      auto fut = sh_prom_ret->get_future().then(  
          [sh_prom_ret, sh_task, func_label, func=std::forward<Func>(func)](auto &x) {
            // ensure all destrutors are called after func completes and from the same persona
            DBG("Returned value ", func_label, "\n");
            return x;
          });
      _pending_fut = when_all(_pending_fut, fut.then([](auto) {}));
      return make_pair(sh_task, fut);
    }
  };

 public:
  static std::atomic<size_t> &global_task_id() {
    static std::atomic<size_t> task_id = 0;
    return task_id;
  }
  static std::atomic<size_t> &global_tasks_completed() {
    static std::atomic<size_t> completed = 0;
    return completed;
  }
  static ThreadPool &get_single_pool(int num_threads = -1);
  static void join_single_pool();
  static auto tasks_outstanding() {
    auto complete = global_tasks_completed().load();
    auto id = global_task_id().load();
    assert(id >= complete);
    return id - complete;
  }

  static void yield_if_needed();
  static void yield();
  static void sleep_ns(uint64_t ns = THREAD_FRIENDLY_POLL_NS);
  static void progress_until(uint64_t ns = THREAD_FRIENDLY_POLL_NS * 1000);

  int get_max_workers() const;
  bool is_done() const;

  template <typename Func>
  static auto enqueue_in_single_pool(int num_workers, Func &&func) {
    auto &tp = ThreadPool::get_single_pool(num_workers);
    return tp.enqueue(std::forward<Func>(func));
  }

  template <typename Func>
  static upcxx::future<> enqueue_in_single_pool_serially(Func &&func) {
    assert(upcxx::master_persona().active_with_caller() && "Called from master persona");
    auto &tp = ThreadPool::get_single_pool();
    return tp.enqueue_serially(std::forward<Func>(func));
  };

  template <typename Func>
  static upcxx::future<> enqueue_in_single_pool_after(upcxx::future<> prerequisite_fut, Func &&func) {
    assert(upcxx::master_persona().active_with_caller() && "Called from master persona");
    auto &tp = ThreadPool::get_single_pool();
    return tp.enqueue_after(prerequisite_fut, std::forward<Func>(func));
  };

  static upcxx::future<> &get_single_pool_serial_future() {
    assert(upcxx::master_persona().active_with_caller() && "Called from master persona");
    return get_single_pool()._serial_fut;
  }

  ThreadPool(int num_threads = 1);
  ThreadPool(const ThreadPool &copy) = delete;
  ThreadPool(ThreadPool &&move) = delete;
  ThreadPool &operator=(const ThreadPool &copy) = delete;
  ThreadPool &&operator=(ThreadPool &move) = delete;
  ~ThreadPool();

  // simply enqueue this in the pool.
  // run immediately if the pool is_done (stopped and empty)
  template <class Func>
  auto enqueue(Func &&func) {
    using result_t = typename std::invoke_result<Func>::type;
    bool run_now = is_done();
#ifdef UPCXX_UTILS_NO_THREAD_POOL
    assert(run_now && "is never active when UPCXX_UTILS_NO_THREAD_POOL");
#endif
    if (run_now) {
      // execute and return immediately
      if constexpr (std::is_void<result_t>::value) {
        func();
        return make_future();
      } else {
        return make_future(func());
      }
    }
    auto [sh_task, fut] = wrap_func(std::forward<Func>(func));
    enqueue_task(sh_task);
    return fut;
  }

  // enqueue after the provided prerequisite_future is ready
  template <typename Func>
  auto enqueue_after(upcxx::future<> prerequisite_future, Func &&func) {
    assert(upcxx::master_persona().active_with_caller() && "Called from master persona");
    DBG_VERBOSE("enqueue_after: ", (prerequisite_future.is_ready() ? "ready" : "NOT READY"), " this=", (void *)this, "\n");

    auto [sh_task, fut] = wrap_func(std::forward<Func>(func));
    auto ret_fut = prerequisite_future.then([sh_task, this]() { this->enqueue_task(sh_task); });
    _pending_fut = when_all(_pending_fut, ret_fut);
    return when_all(ret_fut, fut);
  };

  // enqueue serially using the serialized future chain for this ThreadPool
  template <typename Func>
  upcxx::future<> enqueue_serially(Func &&func) {
    assert(upcxx::master_persona().active_with_caller() && "Called from master persona");
    DBG_VERBOSE("enqueue_serially: ", &_serial_fut, " ", (_serial_fut.is_ready() ? "ready" : "NOT READY"), " this=", (void *)this,
                "\n");

    using return_t = typename std::invoke_result<Func>::type;
    static_assert(std::is_void<return_t>::value, "void is the required return type for enqueue_in_serial_pool");

    _serial_fut = enqueue_after(_serial_fut, std::forward<Func>(func));
    return _serial_fut;
  };

  void join_workers();

  void reset(int num_workers);
};  // class ThreadPool

//
// methods in upcxx_utils namespace
//

// executes in a separate thread in the singleton ThreadPool and sets the maximum worker threads
template <typename Func>
auto execute_in_thread_pool(int num_workers, Func &&func) {
  return ThreadPool::enqueue_in_single_pool(num_workers, std::forward<Func>(func));
};

// executes in a separate thread in the singleton ThreadPool
template <typename Func>
auto execute_in_thread_pool(Func &&func) {
  return execute_in_thread_pool(-1, std::forward<Func>(func));
};

// executes after prerequisite_fut in a separate thread in the singleton ThreadPool
template <typename Func>
auto execute_after_in_thread_pool(upcxx::future<> prerequisite_fut, Func &&func) {
  return ThreadPool::enqueue_in_single_pool_after(prerequisite_fut, std::forward<Func>(func));
};

// executes in a separate thread in the singleton ThreadPool
// All actions will happen as if they were called in series regardless of the size of the ThreadPool
template <typename Func>
upcxx::future<> execute_serially_in_thread_pool(Func &&func) {
  return ThreadPool::enqueue_in_single_pool_serially(std::forward<Func>(func));
};

// Create a new temporary single threaded thread pool with lifetime of the task
template <typename Func>
auto execute_in_new_thread(Func &&func) {
  using result_t = typename std::invoke_result<Func>::type;

  auto sh_tp = make_shared<ThreadPool>(1);
  auto fut = sh_tp->enqueue(std::forward<Func>(func));

  // add future with sh_tp reference copy
  if constexpr (std::is_void<result_t>::value)
    return fut.then([sh_tp]() { LOG("Done with execute_in_new_thread\n"); });
  else
    return fut.then([sh_tp](result_t &&res) {
      LOG("Done with execute_in_new_thread with result\n");
      return res;
    });
};  // execute_in_new_thread

};  // namespace upcxx_utils
