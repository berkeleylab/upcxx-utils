#pragma once
#include <functional>
#include <typeinfo>
#include <vector>

#include "upcxx/upcxx.hpp"
#include "upcxx_utils/min_sum_max.hpp"
#include "upcxx_utils/timers.hpp"

using std::function;
using std::pair;
using std::shared_ptr;
using std::string;
using std::vector;

using upcxx::dist_object;
using upcxx::intrank_t;
using upcxx::make_future;
using upcxx::op_fast_add;
using upcxx::op_fast_max;
using upcxx::op_fast_min;
using upcxx::promise;
using upcxx::team;
using upcxx::when_all;
using upcxx::world;

#ifndef MAX_PROMISE_REDUCTIONS
#ifdef DEBUG
#define MAX_PROMISE_REDUCTIONS (1 << 4)  // 16
#else
#define MAX_PROMISE_REDUCTIONS (1 << 12)  // 4096
#endif
#endif

// #define LOG_PROMISES LOG
#define LOG_PROMISES(...)

namespace upcxx_utils {

int roundup_log2(uint64_t n);

class PromiseBarrier {
  // a two step barrier with delayed execution
  //
  // construct in the master thread (with strict ordering of collectives on team)
  // fulfill() and get_future() methods are safe to call within the restricted context
  // of a progress callback
  //
  // implements the Dissemination Algorithm
  //   detailed in "Two algorithms for Barrier Synchronization"
  //   Manber et al, 1998
  // O(logN) latency
  // O(NlogN) total small messages
  //
  // Usage:
  // PromiseBarrier prom_barrier(team);
  // ...
  // prom_barrier.fulfill();  // may call within progess() callback
  // ...
  // prom_barrier.get_future().then(...); // may call within progress() callback
  //
  struct DisseminationWorkflow;
  using DistDisseminationWorkflow = dist_object<DisseminationWorkflow>;
  struct DisseminationWorkflow {
    static void init_workflow(DistDisseminationWorkflow &dist_dissem);

    DisseminationWorkflow();
    upcxx::future<> get_future() const;

    vector<upcxx::promise<>> level_proms;  // one for each level instance
    upcxx::promise<> initiated_prom;       // to signal this rank start
    upcxx::future<> done_future;
  };

  const upcxx::team &tm;
  DistDisseminationWorkflow dist_workflow;
  bool moved = false;

 public:
  PromiseBarrier(const upcxx::team &tm = upcxx::world());
  PromiseBarrier(PromiseBarrier &&move);
  PromiseBarrier(const PromiseBarrier &copy) = delete;
  PromiseBarrier &operator=(PromiseBarrier &&move);
  PromiseBarrier &operator=(const PromiseBarrier &copy) = delete;
  ~PromiseBarrier();
  void fulfill() const;
  upcxx::future<> get_future() const;
};  // class PromiseBarrier

//
// PromiseBroadcast
//

template <typename T>
struct BroadcastWorkflow {
  static_assert(std::is_trivial<T>::value, "T must be trivial");
  using Data = std::pair<T *, size_t>;
  using DataView = upcxx::view<T>;

  BroadcastWorkflow(const upcxx::team &tm, int root)
      : tm(tm)
      , root(root)
      , local_data_allocated_prom()  // the location of the local Data. requires fullfill_result
      , local_data_ready_prom(1)     // the local Data arrived. requires 1 anonymous
      , view_prom()                  // requires just fullfill_result
  {
    assert(tm.rank_n() > 0);
    assert(root < tm.rank_n());
    assert(root >= 0);
    levels = roundup_log2(tm.rank_n());
  }
  ~BroadcastWorkflow() { DBG_VERBOSE("Deconstruct BroadcastWorkflow\n"); }
  void reset() {
    local_data_allocated_prom = promise<Data>();
    local_data_ready_prom = promise<>(1);
    view_prom = promise<DataView>();
  }
  int get_rotated_rank() const { return (tm.rank_me() + tm.rank_n() - root) % tm.rank_n(); }
  int get_step(int level) const {
    assert(level >= 0 && level < levels);
    int step = 1 << (levels - level - 1);
    DBG_VERBOSE("Step ", step, " level ", level, " ", (void *)this, "\n");
    return step;
  }
  bool is_receiving(int level) const {
    int rrank = get_rotated_rank();
    int step = get_step(level);
    bool ret = (rrank % step == 0 && rrank % (step << 1) != 0);
    DBG_VERBOSE("Is ", ret ? "" : "not", " receiving level ", level, " rrank=", rrank, " ", (void *)this, "\n");
    return ret;
  }
  int next_rank(int level) const {
    int rrank = get_rotated_rank();
    int step = get_step(level);
    int next = rrank + step;
    if (next < tm.rank_n() && rrank % (step << 1) == 0) {
      return (next + root) % tm.rank_n();
    }
    return -1;  // not sending
  }
  bool is_sending(int level) const { return next_rank(level) >= 0; }
  bool am_root() const { return tm.rank_me() == root; }

  const int root;
  const upcxx::team &tm;
  int levels;
  promise<Data> local_data_allocated_prom;
  promise<> local_data_ready_prom;
  promise<DataView> view_prom;
};  // BroadcastWorkflow

template <typename T>
class PromiseBroadcast {
  // a two step broadcast with delayed execution

  // construct in the master thread (with strict ordering of collectives on team)
  // fulfill() and get_future() methods are safe to call within the restricted context
  // of a progress callback
  // can be reused with reset() function, also possible to call within the restricted context

  static_assert(std::is_trivial<T>::value, "T must be trivial");

 public:
  using BWF = BroadcastWorkflow<T>;
  using DistBroadcastWorkflow = dist_object<BWF>;
  using Data = typename BWF::Data;
  using DataView = typename BWF::DataView;

  PromiseBroadcast(int root = 0, const upcxx::team &tm = upcxx::world())
      : sh_dist_workflow(make_shared<DistBroadcastWorkflow>(tm, tm, root))
      , completed(make_future()) {
    reset();
  }

  void reset() {
    assert(upcxx::master_persona().active_with_caller() && "Called from master persona");
    assert(!upcxx::in_progress() && "Not called within the restricted context");

    if (!completed.is_ready()) DIE("Cannot reset until previous invocation has completed\n");
    DistBroadcastWorkflow &dbwf = *sh_dist_workflow;
    dbwf->reset();
    auto root = dbwf->root;
    auto rrank = dbwf->get_rotated_rank();
    bool will_send = rrank % 2 == 0 && rrank != dbwf->tm.rank_n() - 1;  // only even rotated ranks will send rpc

    DBG("Constructed for ", dbwf.id(), " root=", root, " rrank=", rrank, " will_send=", will_send, " ranks=", dbwf->tm.rank_n(),
        " BWF=", (void *)&(*(*sh_dist_workflow)), "\n");

    auto fut_ld_ready =
        dbwf->local_data_ready_prom.get_future().then([&dbwf]() { DBG("Local data is ready for ", dbwf.id(), "\n"); });
    auto fut_ld_allocated = dbwf->local_data_allocated_prom.get_future().then([&dbwf](auto &d) {
      DBG_VERBOSE("Local data is allocated for ", dbwf.id(), " ", (void *)d.first, " count=", d.second, "\n");
      return d;
    });
    auto fut_local_data = when_all(fut_ld_ready, fut_ld_allocated);
    completed = when_all(completed, fut_local_data.then([&dbwf, sh_dbwf = this->sh_dist_workflow](const auto &ignored) {
      DBG_VERBOSE("Local data is allocated and ready for ", dbwf.id(), "\n");
    }));

    bool will_receive = rrank != 0;  // all ranks but root
    if (will_receive) {
      // todo add assertions
    }
    if (will_send) {
      // prepare the view after the local data is allocated and ready
      auto fut_view_ready = fut_local_data.then([&dbwf, sh_dbwf = this->sh_dist_workflow](const Data &data) {
        auto data_view = upcxx::make_view(data.first, data.first + data.second);
        dbwf->view_prom.fulfill_result(data_view);
        DBG_VERBOSE("View ready for ", dbwf.id(), " count=", data.second, "\n");
      });
      completed = when_all(completed, fut_view_ready);

      // iterate over levels sending to me + step, if still within the team
      for (int level = 0; level < dbwf->levels; level++) {
        int step = dbwf->get_step(level);
        int next = dbwf->next_rank(level);
        bool is_sending = next >= 0;
        if (is_sending) {
          DBG_VERBOSE("Will send rpc to ", next, " for ", dbwf.id(), "\n");
          auto rpc_lambda = [next, &dbwf, sh_dbwf = this->sh_dist_workflow](const DataView &data_view) {
            DBG_VERBOSE("Sending rpc to ", next, " data_view.size=", data_view.size(), " for ", dbwf.id(), "\n");
            rpc_ff(
                dbwf->tm, next,
                [](DistBroadcastWorkflow &dbwf, DataView data_view) {
                  auto fut_copied = dbwf->local_data_allocated_prom.get_future().then([&dbwf, data_view](Data data) {
                    assert(data.second == data_view.size());
                    memcpy(data.first, data_view.begin(), data_view.size() * sizeof(T));
                    assert(dbwf->local_data_allocated_prom.get_future().is_ready() && "Local data is allocated");
                    dbwf->local_data_ready_prom.fulfill_anonymous(1);
                    DBG_VERBOSE("Received rpc local data ready on ", dbwf.id(), "\n");
                  });
                  return fut_copied;
                },
                dbwf, data_view);
          };
          auto fut_rpc = dbwf->view_prom.get_future().then(rpc_lambda);
          completed = when_all(fut_rpc, completed);
        }
      }
    }
    // preserve the dist_workflow until it is all done
    completed = completed.then([sh_dbwf = this->sh_dist_workflow]() {
      DistBroadcastWorkflow &dbwf = *sh_dbwf;
      DBG_VERBOSE("PromiseBroadcast completed for ", dbwf.id(), "\n");
    });
  }

  PromiseBroadcast(const PromiseBroadcast &copy) = delete;
  PromiseBroadcast(PromiseBroadcast &&move) = delete;
  PromiseBroadcast &operator=(const PromiseBroadcast &copy) = delete;
  PromiseBroadcast &operator=(PromiseBroadcast &&move) = delete;

  upcxx::future<> get_future() const { return completed; }

  void fulfill(Data data) {
    assert(upcxx::master_persona().active_with_caller() && "Called from master persona");
    auto &dbwf = *sh_dist_workflow;
    DBG_VERBOSE("Fulfilling for ", dbwf.id(), " ", (void *)data.first, " count=", data.second, "\n");
    assert(!dbwf->local_data_allocated_prom.get_future().is_ready());
    assert(!dbwf->local_data_ready_prom.get_future().is_ready());
    assert(!completed.is_ready());
    dbwf->local_data_allocated_prom.fulfill_result(data);
    assert(dbwf->local_data_allocated_prom.get_future().is_ready());
    if (dbwf->root == dbwf->tm.rank_me()) dbwf->local_data_ready_prom.fulfill_anonymous(1);
  }

  // convenience methods for to fulfill
  void fulfill(T *data, size_t count) {
    Data d{data, count};
    fulfill(d);
  }
  void fulfill(T *begin, T *end) {
    Data d{begin, end - begin};
    fulfill(d);
  }
  void fulfill(T &val) { fulfill(&val, 1); }
  void fulfill(upcxx::future<Data> fut_data) {
    assert(upcxx::master_persona().active_with_caller() && "Called from master persona");
    completed = when_all(completed, fut_data.then([this](Data d) {
      auto &dbwf = *(this->sh_dist_workflow);
      DBG_VERBOSE("Fulfilling for future data for ", dbwf.id(), "\n");
      this->fulfill(d);
    }));
  }

  template <typename Bag>
  void fulfill(Bag &bag) {
    auto &dbwf = *sh_dist_workflow;
    auto sh_vec = make_shared<std::vector<T>>(bag.begin(), bag.end());
    Data d(sh_vec->data(), sh_vec->size());
    if (!dbwf->am_root()) {
      completed = completed.then([sh_vec, &bag, &dbwf, sh_dbwf = this->sh_dist_workflow]() {
        assert(bag.size() == sh_vec->size());
        std::copy(sh_vec->begin(), sh_vec->end(), bag.begin());
        DBG_VERBOSE("Copied back to original container for ", dbwf.id(), "\n");
      });
    }
    fulfill(d);
  };

 private:
  shared_ptr<DistBroadcastWorkflow> sh_dist_workflow;
  upcxx::future<> completed;
};  // PromiseBroadcast

// PromiseReduce functor

struct op_PromiseReduce {
  using T = int64_t;
  using Func = std::function<T(T, T)>;
  using Funcd = std::function<double(double, double)>;
  using BothFunc = std::pair<Func, Funcd>;
  using FuncBool = std::pair<BothFunc, bool>;
  using Funcs = vector<FuncBool>;
  Funcs &_ops;

  // get_op() rotates through the operations each time operator() is called
  // FuncBool &get_op() const { return _ops[_rrobin++ % _ops.size()]; }
  op_PromiseReduce(Funcs &ops);
  op_PromiseReduce() = delete;
  op_PromiseReduce(const op_PromiseReduce &copy);
  static const double &T2double(const T &x);
  static const T &double2T(const double &x);

  // Each operation will be applied to an array where each operation could be different
  template <typename Ta, typename Tb>
  Ta operator()(Ta &__a, Tb &&__b) const {
    T *_a = const_cast<T *>(__a.data());
    T *_b = const_cast<T *>(__b.data());
    // DBG_VERBOSE("Operating on ", _ops.size(), " elements at ", (void*) _a, " ", (void*) _b, " this=", (void*) this, "\n");
    for (int i = 0; i < _ops.size(); i++) {
      T &a = _a[i];
      T &b = _b[i];
      FuncBool &op = _ops[i];
      // DBG_VERBOSE("Operating ", typeid(op.first).name(), " ", op.second ? " double " : " long ", " on a=", a, " b=", b, "\n");
      if (op.second) {
        const double &a_d = T2double(a);
        const double &b_d = T2double(b);
        const double a_d2 = op.first.second(a_d, b_d);
        // DBG_VERBOSE("a_d=", a_d, " b_d=", b_d, " a_d2=", a_d2, "\n");
        a = double2T(a_d2);
      } else {
        a = op.first.first(a, std::forward<T>(b));
      }
      // DBG_VERBOSE("Result a=", a, "\n");
    }
    return static_cast<Ta &&>(__a);
  };
};  // struct op_PromiseReduce

class PromiseReduce {
  // PromiseReduce
  //
  // a class to consolidate and delay a series of arbitrary reductions.
  // All values are converted to uint64_t (doubles are first bit-smashed into an int64_t and un-smashed when operating on and
  // returned)
  //   bit smashing is facilitated by op_PromiseReduce::T2double and op_PromiseReduce::double2T
  //   The aggregate return contains the double bit-smashed into int64_t, but the individual return type is the original type that
  //   reduce_* was called on.
  // Any series of binary operators is allowed
  //   ranks may initiate reduce from within the restricted contex
  //   **BUT** the order of reduce_one and reduce_all calls must be consistant across ranks
  // if any of the ops are reduce_all then all the values are broadcasted after the reduction.
  // or if all the ops are reduce_one and the root is not rank0 for a given operation, then a rpc is issued to get the value to
  // the correct rank
  //
  // fulfill can be called any number of times, and trigger a reduction for all pending reduce_one and reduce_all operations
  // fulfill can also not be called at all and will trigger all reductions before the PromiseReduce instance is destroyed
  //
  // Usage:
  //  PromiseReduce pr(local_team());  // create a PromiseReduce working on the local_team
  //  auto fut1 = pr.reduce_one(rank_me(), op_fast_max, 0); // max to rank 0
  //  auto fut2 = pr.reduce_one(rank_me(), op_fast_add, local_team().rank_n()-1); // sum to last rank on node
  //  auto fut3 = pr.reduce_all(rank_me(), op_fast_add); // sum to all ranks
  //  auto fut4 = pr.reduce_all(1.0/rank_me(), op_fast_add); // sum double to all ranks
  //  auto fut_4_reductions = pr.fulfill();
  //  assert(fut_4_redutions.wait().size() == 4);
  //  auto [ int1, int2, int3, double4 ] = fut_4_reductions.wait();
  //  assert(fut1.wait() == int1);
  //  assert(fut2.wait() == int2);
  //  assert(fut3.wait() == int3);
  //  assert(fabs(fut4.wait() - op_PromiseReduce::T2double(double4)) < 0.001);
  //  pr.reduce_all(rank_me(), op_fast_add);
  //  auto fut1_reduction = pr.fulfill(); // instance can be used to initate more reductions
  //  assert(fut_reduction.wait().size() == 1);

 public:
  using T = op_PromiseReduce::T;
  using Funcs = op_PromiseReduce::Funcs;
  using Promises = vector<shared_ptr<promise<T>>>;
  using Vals = vector<T>;

 protected:
  Vals _vals;
  const upcxx::team &tm;
  op_PromiseReduce::Funcs _ops;
  vector<int> _roots;
  Promises _proms;
  upcxx::future<> _vals_ready;

 public:
  PromiseReduce(const team &_team = world());

  ~PromiseReduce();

  static size_t &get_global_count() {
    static size_t _ = 0;
    return _;
  }

  static size_t &get_fulfilled_count() {
    static size_t _ = 0;
    return _;
  }

  const upcxx::team &get_team() const { return tm; }

  template <typename ValType, typename Op>
  upcxx::future<ValType> reduce_one(ValType orig_val, Op &op, int root = 0) {
    assert(!upcxx::in_progress() && "Not called within the restricted context");
    assert(upcxx::master_persona().active_with_caller());
    T val;
    bool is_float = false;
    if constexpr (std::is_floating_point<ValType>::value) {
      // val = (T)(orig_val * TRANSFORM_FLOAT);
      double conv_val = orig_val;  // upscale floats to doubles
      val = op_PromiseReduce::double2T(conv_val);
      is_float = true;
    } else
      val = orig_val;

    LOG_PROMISES("Added reduce_", (root < 0 ? "all" : "one"), " op #", _vals.size(), " ", typeid(op).name(), " ", (void *)&op,
                 "on ", typeid(ValType).name(), " val=", val, " orig_val=", orig_val, ". global_count=", get_global_count(),
                 " fulfilled_count=", get_fulfilled_count(), " this=", (void *)this, "\n");
    auto gc = get_global_count()++;
    static size_t MAX_RESERVE = MAX_PROMISE_REDUCTIONS;
    MAX_RESERVE = std::max(MAX_RESERVE, MAX_PROMISE_REDUCTIONS * (_vals.size() + MAX_PROMISE_REDUCTIONS) / MAX_PROMISE_REDUCTIONS);
    auto sh_prom = make_shared<promise<T>>();
    _vals.reserve(MAX_RESERVE);
    _proms.reserve(MAX_RESERVE);
    _ops.reserve(MAX_RESERVE);
    _roots.reserve(MAX_RESERVE);
    _vals.push_back(val);
    _proms.push_back(sh_prom);
    _ops.push_back({{op, op}, is_float});
    _roots.push_back(root);
    auto fut_ret = sh_prom->get_future().then([&self = *this, sh_prom, is_float, sz = _vals.size(), root](const T &result) {
      ValType conv_result = result;
      if (std::is_floating_point<ValType>::value) {
        assert(is_float);
        double double_result = op_PromiseReduce::T2double(result);
        conv_result = double_result;  // downscale doubles to floats if needed
        DBG_VERBOSE("Converted reduce_", (root < 0 ? "all" : "one"), " op #", sz - 1, " from ", result, " back to ", conv_result,
                    "\n");
      } else {
        assert(!is_float);
      }
      DBG_VERBOSE("Returning ", conv_result, " sh_prom:", (void *)sh_prom.get(), "\n");
      return conv_result;
    });
    DBG_VERBOSE("promise reduce_one on ", (void *)this, " pending: gc=", gc, " sz=", _vals.size(),
                " global_count=", get_global_count(), " fulfilled_count=", get_fulfilled_count(), "\n");
    upcxx::progress();
    return fut_ret;
  };

  template <typename ValType, typename Op>
  upcxx::future<> reduce_one(ValType *in_vals, ValType *out_vals, int count, Op &op, int root = 0) {
    upcxx::future<> chain_fut = make_future();
    for (int i = 0; i < count; count++) {
      auto fut_val = reduce_one(in_vals[i], op, root).then([&out = out_vals[i]](const ValType &val) { return out = val; });
      chain_fut = when_all(chain_fut, fut_val.then([](const ValType &ignore) {}));
    }
    return chain_fut;
  };

  template <typename ValType, typename Op>
  upcxx::future<ValType> reduce_all(ValType orig_val, Op &op) {
    return reduce_one(orig_val, op, -1);
  };

  template <typename ValType, typename Op>
  upcxx::future<> reduce_all(ValType *in_vals, ValType *out_vals, int count, Op &op) {
    return reduce_one(in_vals, out_vals, count, op, -1);
  };

  template <typename ValType, typename Op>
  upcxx::future<ValType> fut_reduce_one(upcxx::future<ValType> fut_orig_val, Op &op, int root = 0) {
    assert(!upcxx::in_progress() && "Not called within the restricted context");
    assert(upcxx::master_persona().active_with_caller());
    _vals_ready = when_all(_vals_ready, fut_orig_val.then([](auto ignored) {}));
    auto gc = get_global_count();
    auto fc = get_fulfilled_count();
    assert(gc >= fc);
    auto o = _vals.size();
    assert(o == gc - fc);
    auto fut_ret = reduce_one((ValType)0, op, root);
    assert(gc + 1 == get_global_count());
    assert(_vals.back() == (ValType)0);
    assert(o == _vals.size() - 1);
    assert(o == &_vals.back() - &_vals.front());
    auto fut_val_set = fut_orig_val.then([&self = *this, gc, fc, o](ValType new_val) {
      assert(upcxx::master_persona().active_with_caller());
      // _vals is safe to access here, since _vals_ready will be waited before _vals is consumed during fulfill
      // so capture the global_count and subtract the fulfilled count to find the offset
      assert(gc <= get_global_count() && "global_count is monotonic");
      assert(fc == get_fulfilled_count() && "PromiseReduce::fulfill() has not been called before setting the val to be reduced");
      auto offset = gc - fc;
      DBG_VERBOSE("Setting val before reduction at gc=", gc, " sz=", self._vals.size(), " fc=", fc, " o=", o, " offset=", offset,
                  " global_count=", get_global_count(), " fulfull_count=", get_fulfilled_count(), " new_val=", new_val,
                  " _vals.data()=", (void *)self._vals.data(), "\n");
      assert(offset >= 0);
      assert(offset < self._vals.size() && "offset fits within _vals");
      assert(offset == o);
      auto &val = self._vals[offset];
      if constexpr (std::is_floating_point<ValType>::value) {
        double conv_val = new_val;  // upscale floats to doubles
        val = op_PromiseReduce::double2T(conv_val);
      } else
        val = new_val;
    });
    DBG("fut_reduce_one pending gc=", gc, " sz=", _vals.size(), " fut_orig_val:", fut_orig_val.is_ready(), " _vals_ready before ",
        _vals_ready.is_ready(), "\n");
    _vals_ready = when_all(_vals_ready, fut_val_set);
    return fut_ret;
  }

  template <typename ValType, typename Op>
  upcxx::future<ValType> fut_reduce_all(upcxx::future<ValType> fut_orig_val, Op &op) {
    return fut_reduce_one(fut_orig_val, op, -1);
  }

  template <typename ValType>
  upcxx::future<MinSumMax<ValType>> fut_msm_reduce_one(upcxx::future<ValType> fut_orig_val, upcxx::future<bool> fut_is_active,
                                                       int root = 0) {
    assert(!upcxx::in_progress());
    assert(upcxx::master_persona().active_with_caller());
    assert(root == -1 || root < get_team().rank_n());
    auto gc = get_global_count();
    fut_orig_val = fut_orig_val.then([gc](auto &v) {
      DBG_VERBOSE("gc=", gc, " fut val ready: ", v, "\n");
      return v;
    });
    auto fut_my = fut_orig_val;
    auto fut_min = fut_reduce_one(fut_orig_val, op_fast_min, root);
    auto fut_sum = fut_reduce_one(fut_orig_val, op_fast_add, root);
    auto fut_max = fut_reduce_one(fut_orig_val, op_fast_max, root);
    auto fut_is_active_to_int = fut_is_active.then([](bool is_active) { return (int)(is_active ? 1 : 0); });
    auto fut_active = fut_reduce_one(fut_is_active_to_int, op_fast_add, root);
    DBG("fut_msm_reduce_one pending gc=", gc, " sz=", _vals.size(), " fut_orig_val: ", fut_orig_val.is_ready(), "\n");
    return when_all(fut_my, fut_min, fut_sum, fut_max, fut_active, fut_is_active)
        .then([this, root](const ValType &my, const ValType &min, const ValType &sum, const ValType &max, int active_ranks,
                           bool is_active) -> MinSumMax<ValType> {
          DBG_VERBOSE("my=", my, " min=", min, " max=", max, " active_ranks=", active_ranks, " is_active=", is_active,
                      " root=", root, "\n");
          MinSumMax<ValType> msm{};
          msm.my = my;
          msm.min = min;
          msm.sum = sum;
          msm.max = max;
          assert(root != this->get_team().rank_me() || (active_ranks >= 0 && active_ranks <= this->get_team().rank_n()));
          msm.active_ranks = active_ranks;
          msm.apply_avg();
          return msm;
        });
  };

  template <typename ValType>
  upcxx::future<MinSumMax<ValType>> msm_reduce_one(ValType orig_val, bool is_active, int root = 0) {
    return fut_msm_reduce_one(make_future(orig_val), make_future(is_active), root);
  }

  template <typename ValType>
  upcxx::future<MinSumMax<ValType>> msm_reduce_one(ValType orig_val, int root = 0) {
    return msm_reduce_one(orig_val, true, root);
  };

  template <typename ValType>
  upcxx::future<MinSumMax<ValType>> msm_reduce_all(ValType orig_val, bool is_active = true) {
    return msm_reduce_one(orig_val, is_active, -1);
  };

  template <typename T>
  upcxx::future<> msm_reduce_one(const MinSumMax<T> *msm_in, MinSumMax<T> *msm_out, int count, intrank_t root = 0) {
    upcxx::future<> chain_fut = make_future();
    for (int i = 0; i < count; i++) {
      auto &out = msm_out[i];
      auto &in = msm_in[i];
      assert(in.my == in.min && "msm is properly initialized");
      assert(in.my == in.max && "msm is properly initialized");
      assert(in.my == in.sum && "msm is properly initialized");
      auto fut_reduce = msm_reduce_one(in.my, in.active_ranks >= 1, root);
      auto fut_msm = fut_reduce.then([&in, &out](const MinSumMax<T> &msm) { out = msm; });
      chain_fut = when_all(chain_fut, fut_msm);
    }
    return chain_fut;
  };

  template <typename T>
  upcxx::future<> msm_reduce_all(const MinSumMax<T> *msm_in, MinSumMax<T> *msm_out, int count) {
    return msm_reduce_one(msm_in, msm_out, count, -1);
  }

  template <typename ValType>
  upcxx::future<MinSumMax<ValType>> fut_msm_reduce_all(upcxx::future<ValType> fut_orig_val, upcxx::future<bool> fut_is_active) {
    return fut_msm_reduce_one(fut_orig_val, fut_is_active, -1);
  }

  template <typename ValType>
  upcxx::future<> fut_msm_reduce_one(upcxx::future<> fut_my_msm_ready, const MinSumMax<ValType> *msm_in,
                                     MinSumMax<ValType> *msm_out, int count, upcxx::future<bool> fut_is_active,
                                     intrank_t root = 0) {
    assert(!upcxx::in_progress() && "Not called within the restricted context");
    auto my_vals_ready = when_all(fut_my_msm_ready, fut_is_active.then([](auto) {}));
    _vals_ready = when_all(my_vals_ready, _vals_ready);
    auto fut_chain = _vals_ready;
    for (int i = 0; i < count; i++) {
      auto fut_val = my_vals_ready.then([&in = msm_in[i]]() {
        assert(in.my == in.min && "msm is fully initialized");
        assert(in.my == in.max && "msm is fully initialized");
        assert(in.my == in.sum && "msm is fully initialized");
        return in.my;
      });
      auto fut_reduce = fut_msm_reduce_one(fut_val, fut_is_active, root);
      auto fut_ret = fut_reduce.then([=, &out = msm_out[i]](MinSumMax<ValType> msm) { out = msm; });
      fut_chain = when_all(fut_chain, fut_ret);
    }
    return fut_chain;
  };

  upcxx::future<> fulfill();
  upcxx::future<> bulk_reduce(shared_ptr<Vals> sh_vals, shared_ptr<Vals> sh_results, shared_ptr<Funcs> sh_ops,
                              bool requires_broadcast);

};  // class PromiseReduce

};  // namespace upcxx_utils
