#pragma once
// reget_irreg.hpp

#include <cassert>
#include <algorithm>
#include <vector>

#include <upcxx/upcxx.hpp>

#include "upcxx_utils/log.hpp"
#include "upcxx_utils/split_rank.hpp"

using std::vector;
using upcxx::make_future;
using upcxx::make_view;

namespace upcxx_utils {

template <typename SrcIter, typename DestIter>
upcxx::future<> rget_irregular_by_node(SrcIter src_begin, SrcIter src_end, DestIter dest_begin, DestIter dest_end) {
  using GU = typename std::tuple_element<0, typename std::iterator_traits<SrcIter>::value_type>::type;
  using U = typename GU::element_type;
  using T = typename std::remove_const<U>::type;
  using D = typename std::tuple_element<0, typename std::iterator_traits<DestIter>::value_type>::type;

  static_assert(upcxx::is_trivially_serializable<T>::value, "RMA operations only work on TriviallySerializable types.");

  static_assert(std::is_convertible<D, const T *>::value, "SrcIter and DestIter need to be over same base T type");

  using V = vector<T>;

  // verify all global_ptr are from the same node
  int src_node = -1;
  size_t src_size = 0, src_count = 0, dest_size = 0;
  for (auto src_i = src_begin; src_i != src_end; src_i++) {
    auto &src = *src_i;
    GU &gptr = src.first;
    auto node = gptr.where() / local_team().rank_n();
    if (src_node == -1)
      src_node = node;
    else if (src_node != node)
      DIE("rget_irregular_by_node called with pointer to multiple nodes! ", src_node, " for ", (*src_begin).first, " vs ", node,
          " for ", gptr, "\n");
    src_size += src.second * sizeof(T);
    src_count += src.second;
  }
  if (src_node == -1) {
    WARN("rget_irregular called with no src pointers to get!\n");
    return make_future();
  }

  // verify sum of src and dest are equal
  for (auto dest_i = dest_begin; dest_i != dest_end; dest_i++) {
    auto &dest = *dest_i;
    D &ptr = dest.first;
    if (ptr == nullptr) DIE("Cannot use nullptr as dest");
    dest_size += dest.second * sizeof(T);
  }
  if (dest_size != src_size) DIE("rget_irregular_by_node has a mismatch in src (", src_size, ") and dest (", dest_size, ") sizes");

  // rpc to remote node
  auto fut_result = upcxx::rpc(
      upcxx_utils::split_rank::node_team(), src_node,
      [](auto src_view) -> V {
        V results;
        size_t count = 0;
        for (auto &gptr_sz : src_view) {
          auto &[gptr, sz] = gptr_sz;
          if (!gptr.is_local()) DIE("non-local gptr ", gptr);
          count += sz;
        }
        results.reserve(count);
        DBG("rget_irregular_by_node - Responding with ", count, " results\n");
        for (auto &gptr_sz : src_view) {
          auto &[gptr, sz] = gptr_sz;
          results.insert(results.end(), gptr.local(), gptr.local() + sz);
        }
        assert(results.size() == count);
        return results;
      },
      make_view(src_begin, src_end));

  // complete copy to the local dest
  upcxx::future<> fut_ret = fut_result.then([=](const V &v) {
    auto v_i = v.begin();
    for (auto dest_i = dest_begin; dest_i != dest_end; dest_i++) {
      assert(v_i != v.end());
      auto &d = *dest_i;
      auto &dp = d.first;
      auto &dsz = d.second;
      std::copy(v_i, v_i + dsz, dp);
#ifdef DEBUG
      for (int i = 0; i < dsz; i++) {
        assert(v_i != v.end());
        v_i++;
      }
#else
      v_i += dsz;
#endif
    }
    if (v_i != v.end()) DIE("Did not use all the results");
  });

  return fut_ret;
};  // rget_irregular_by_node

};  // namespace upcxx_utils