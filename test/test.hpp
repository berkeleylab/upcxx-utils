#pragma once

#include <cassert>
#include "upcxx_utils/log.hpp"

// macro to ensure that even Release builds will be properly testing
#define ASSERT(...)                            \
  do {                                         \
    assert(__VA_ARGS__);                       \
    if (!(__VA_ARGS__)) {                      \
      DIE("Invalid assertion: '", #__VA_ARGS__, "'"); \
    }                                          \
  } while (0);

  