
#include <vector>
#include <random>

#include <upcxx/upcxx.hpp>

#include "test.hpp"
#include "upcxx_utils/rget_irreg.hpp"
#include "upcxx_utils/split_rank.hpp"
#include "upcxx_utils/log.hpp"

using std::vector;

using upcxx::allocate;
using upcxx::barrier;
using upcxx::dist_object;
using upcxx::future;
using upcxx::global_ptr;
using upcxx::make_future;
using upcxx::rank_me;
using upcxx::rank_n;

using namespace upcxx_utils;

int test_rget_irreg(int argc, char **argv) {
  upcxx_utils::open_dbg("test_rget_irreg");
  
  // prepare global_ptrs
  int num = 1000;
  dist_object<global_ptr<int>> dist_ptr({});
  (*dist_ptr) = allocate<int>(num);
  int *ptr = dist_ptr->local();
  for (int i = 0; i < num; i++) {
    ptr[i] = rank_me() * num + i;
  }
  barrier();

  std::mt19937 g(rank_me());  // seed based on rank_me
  vector<int> lookups, dest(num * 5, 0);
  lookups.reserve(num);
  for (int i = 0; i < num; i++) lookups.push_back(i);
  std::shuffle(lookups.begin(), lookups.end(), g);

  using G = std::pair<global_ptr<int>, int>;
  using D = std::pair<int *, int>;

  vector<G> src_list(num / 2);
  vector<D> dest_list(num / 2);

  // cache each ranks global_ptr locally
  vector<global_ptr<int>> rank_ptrs(rank_n());
  upcxx::future<> fut_chain = make_future();
  for (auto rank : foreach_rank()) {
    auto fut = dist_ptr.fetch(rank).then([&, rank](auto x) { rank_ptrs[rank] = x; });
    fut_chain = when_all(fut_chain, fut);
  }
  fut_chain.wait();
  barrier();

  fut_chain = make_future();

  for (auto rank : foreach_rank()) {
    // retrieve a smattering of values from a rank
    src_list.clear();
    dest_list.clear();
    auto &rank_ptr = rank_ptrs[rank];
    for (int i = 0; i < num / 2; i++) {
      auto x = lookups[i];
      auto sz = num - x > 5 ? 5 : num - x;  // prevent overruns
      src_list.emplace_back(rank_ptr + x, sz);
      dest_list.emplace_back(dest.data() + i * 5, sz);
    }
    rget_irregular_by_node(src_list.begin(), src_list.end(), dest_list.begin(), dest_list.end()).wait();
    for (int i = 0; i < num / 2; i++) {
      auto x = lookups[i];
      auto sz = src_list[i].second;
      ASSERT(sz == dest_list[i].second);
      for (int j = 0; j < sz; j++) {
        auto val = rank * num + x + j;
        if (val != dest_list[i].first[j])
          DIE("Invalid val rank=", rank, " x=", x, " i=", i, " j=", j, " sz=", sz, " val=", val,
              " dest_list[i][j]=", dest_list[i].first[j]);
      }
    }
  }

  barrier();

  auto l_n = upcxx::local_team().rank_n();
  auto &nt = upcxx_utils::split_rank::node_team();
  for (auto node : foreach_rank(nt)) {
    // retrieve a smattering of values from a node
    // retrieve a smattering of values from a rank
    src_list.clear();
    dest_list.clear();
    vector<int> ranks;
    ranks.reserve(num / 2);
    for (int i = 0; i < num / 2; i++) {
      auto random_rank = node * l_n + rand() % l_n;
      ranks.push_back(random_rank);
      auto &rank_ptr = rank_ptrs[random_rank];
      auto x = lookups[i];
      auto sz = num - x > 5 ? 5 : num - x;  // prevent overruns
      src_list.emplace_back(rank_ptr + x, sz);
      dest_list.emplace_back(dest.data() + i * 5, sz);
    }
    rget_irregular_by_node(src_list.begin(), src_list.end(), dest_list.begin(), dest_list.end()).wait();
    for (int i = 0; i < num / 2; i++) {
      auto &rank = ranks[i];
      auto x = lookups[i];
      auto sz = src_list[i].second;
      ASSERT(sz == dest_list[i].second);
      for (int j = 0; j < sz; j++) {
        auto val = rank * num + x + j;
        if (val != dest_list[i].first[j])
          DIE("Invalid val rank=", rank, " x=", x, " i=", i, " j=", j, " sz=", sz, " val=", val,
              " dest_list[i][j]=", dest_list[i].first[j]);
      }
    }
  }
  fut_chain.wait();
  barrier();
  upcxx_utils::close_dbg();

  return 0;
}
