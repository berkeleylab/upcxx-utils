#include <random>
#include <vector>

#include "test.hpp"
#include "upcxx/upcxx.hpp"
#include "upcxx_utils/log.hpp"
#include "upcxx_utils/promise_collectives.hpp"

using std::vector;

using upcxx::barrier;
using upcxx::future;
using upcxx::make_future;
using upcxx::when_all;

using upcxx_utils::op_PromiseReduce;
using upcxx_utils::PromiseBarrier;
using upcxx_utils::PromiseBroadcast;
using upcxx_utils::PromiseReduce;
using upcxx_utils::roundup_log2;

#define ASSERT_FLOAT(a, b, c) ASSERT((a) >= ((b) - 0.000001) && (a) <= ((b) + 0.000001) && c)

int test_promise_barrier(int argc, char **argv) {
  barrier();
  upcxx_utils::open_dbg("test_promise_barrier");

  ASSERT(roundup_log2(0) == -1);
  ASSERT(roundup_log2(1) == 0);
  ASSERT(roundup_log2(2) == 1);
  ASSERT(roundup_log2(3) == 2);
  ASSERT(roundup_log2(4) == 2);
  ASSERT(roundup_log2(5) == 3);
  ASSERT(roundup_log2(6) == 3);
  ASSERT(roundup_log2(7) == 3);
  ASSERT(roundup_log2(8) == 3);
  ASSERT(roundup_log2(9) == 4);
  ASSERT(roundup_log2(15) == 4);
  ASSERT(roundup_log2(16) == 4);
  ASSERT(roundup_log2(17) == 5);
  {
    PromiseBarrier pb;
    ASSERT(!pb.get_future().is_ready());
    pb.fulfill();
    pb.get_future().wait();
  }
  barrier();
  {
    DBG("1s 2s 1e 2e\n");
    barrier();
    PromiseBarrier pb1, pb2;
    barrier();
    ASSERT(!pb1.get_future().is_ready());
    pb1.fulfill();
    barrier();
    ASSERT(!pb2.get_future().is_ready());
    pb2.fulfill();
    barrier();
    pb1.get_future().wait();
    barrier();
    pb2.get_future().wait();
    barrier();
  }
  {
    DBG("1s 1e 2s 2e\n");
    barrier();
    PromiseBarrier pb1, pb2;
    barrier();
    ASSERT(!pb1.get_future().is_ready());
    pb1.fulfill();
    barrier();
    pb1.get_future().wait();
    barrier();
    ASSERT(!pb2.get_future().is_ready());
    pb2.fulfill();
    barrier();
    pb2.get_future().wait();
    barrier();
  }
  {
    DBG("1s 2s 2e 1e\n");
    barrier();
    PromiseBarrier pb1, pb2;
    barrier();
    ASSERT(!pb1.get_future().is_ready());
    pb1.fulfill();
    barrier();
    ASSERT(!pb2.get_future().is_ready());
    pb2.fulfill();
    barrier();
    pb2.get_future().wait();
    barrier();
    pb1.get_future().wait();
    barrier();
  }

  {
    DBG("2s 1s 2e 1e\n");
    barrier();
    PromiseBarrier pb1, pb2;
    barrier();
    ASSERT(!pb2.get_future().is_ready());
    pb2.fulfill();
    barrier();
    ASSERT(!pb1.get_future().is_ready());
    pb1.fulfill();
    barrier();
    pb2.get_future().wait();
    barrier();
    pb1.get_future().wait();
    barrier();
  }
  {
    DBG("2s 2e 1s 1e\n");
    barrier();
    PromiseBarrier pb1, pb2;
    barrier();
    ASSERT(!pb2.get_future().is_ready());
    pb2.fulfill();
    barrier();
    pb2.get_future().wait();
    barrier();
    ASSERT(!pb1.get_future().is_ready());
    pb1.fulfill();
    barrier();
    pb1.get_future().wait();
    barrier();
  }
  {
    DBG("2s 1s 1e 2e\n");
    barrier();
    PromiseBarrier pb1, pb2;
    barrier();
    ASSERT(!pb2.get_future().is_ready());
    pb2.fulfill();
    barrier();
    ASSERT(!pb1.get_future().is_ready());
    pb1.fulfill();
    barrier();
    pb1.get_future().wait();
    barrier();
    pb2.get_future().wait();
    barrier();
  }

  std::mt19937 g(rank_n());  // seed all ranks the same
  {
    DBG("fulfill all barrier wait all same order\n");
    int iterations = 1000;
    vector<PromiseBarrier> pbs(iterations);
    vector<intrank_t> fulfill_order(iterations);
    vector<intrank_t> &wait_order = fulfill_order;
    for (int i = 0; i < iterations; i++) {
      fulfill_order[i] = i;
      wait_order[i] = i;
      ASSERT(!pbs[i].get_future().is_ready());
    }
    std::shuffle(fulfill_order.begin(), fulfill_order.end(), g);
    barrier();
    // initiate all
    for (int i = 0; i < iterations; i++) {
      ASSERT(!pbs[fulfill_order[i]].get_future().is_ready());
      pbs[fulfill_order[i]].fulfill();
    }
    // wait all
    future<> all_fut = make_future();
    for (int i = 0; i < iterations; i++) {
      auto fut = pbs[wait_order[i]].get_future();
      all_fut = when_all(all_fut, fut);
    }
    all_fut.wait();
    barrier();
  }

  {
    DBG("fulfill all barrier wait all different order\n");
    int iterations = 1000;
    vector<PromiseBarrier> pbs(iterations);
    vector<intrank_t> fulfill_order(iterations);
    vector<intrank_t> wait_order(iterations);
    for (int i = 0; i < iterations; i++) {
      fulfill_order[i] = i;
      wait_order[i] = i;
      ASSERT(!pbs[i].get_future().is_ready());
    }
    std::shuffle(fulfill_order.begin(), fulfill_order.end(), g);
    std::shuffle(wait_order.begin(), wait_order.end(), g);
    barrier();
    // initiate all
    for (int i = 0; i < iterations; i++) {
      ASSERT(!pbs[fulfill_order[i]].get_future().is_ready());
      pbs[fulfill_order[i]].fulfill();
    }
    barrier();
    // wait all
    future<> all_fut = make_future();
    for (int i = 0; i < iterations; i++) {
      auto fut = pbs[wait_order[i]].get_future();
      all_fut = when_all(all_fut, fut);
    }
    all_fut.wait();
    barrier();
  }

  {
    DBG("fulfill all barrier wait each same order\n");
    int iterations = 1000;
    vector<PromiseBarrier> pbs(iterations);
    vector<intrank_t> fulfill_order(iterations);
    vector<intrank_t> &wait_order = fulfill_order;
    for (int i = 0; i < iterations; i++) {
      fulfill_order[i] = i;
      wait_order[i] = i;
    }
    std::shuffle(fulfill_order.begin(), fulfill_order.end(), g);
    barrier();
    // initiate all
    for (int i = 0; i < iterations; i++) {
      ASSERT(!pbs[fulfill_order[i]].get_future().is_ready());
      pbs[fulfill_order[i]].fulfill();
    }
    barrier();
    // wait each
    for (int i = 0; i < iterations; i++) {
      pbs[wait_order[i]].get_future().wait();
    }
    barrier();
  }

  {
    DBG("fulfill all barrier wait each different order\n");
    int iterations = 1000;
    vector<PromiseBarrier> pbs(iterations);
    vector<intrank_t> fulfill_order(iterations);
    vector<intrank_t> wait_order(iterations);
    for (int i = 0; i < iterations; i++) {
      fulfill_order[i] = i;
      wait_order[i] = i;
    }
    std::shuffle(fulfill_order.begin(), fulfill_order.end(), g);
    std::shuffle(wait_order.begin(), wait_order.end(), g);
    barrier();
    // initiate all
    for (int i = 0; i < iterations; i++) {
      ASSERT(!pbs[fulfill_order[i]].get_future().is_ready());
      pbs[fulfill_order[i]].fulfill();
    }
    barrier();
    // wait each
    for (int i = 0; i < iterations; i++) {
      pbs[wait_order[i]].get_future().wait();
    }
    barrier();
  }

  {
    DBG("fulfill then wait each same order\n");
    int iterations = 1000;
    vector<PromiseBarrier> pbs(iterations);
    vector<intrank_t> fulfill_order(iterations);
    vector<intrank_t> &wait_order = fulfill_order;
    for (int i = 0; i < iterations; i++) {
      fulfill_order[i] = i;
      wait_order[i] = i;
    }
    std::shuffle(fulfill_order.begin(), fulfill_order.end(), g);
    barrier();
    // initiate all
    for (int i = 0; i < iterations; i++) {
      ASSERT(!pbs[fulfill_order[i]].get_future().is_ready());
      pbs[fulfill_order[i]].fulfill();
      pbs[wait_order[i]].get_future().wait();
    }
    barrier();
  }

  DBG("fulfill then wait each different order would deadlock\n");

  {
    DBG("fulfill all then wait all same order\n");
    int iterations = 1000;
    vector<PromiseBarrier> pbs(iterations);
    vector<intrank_t> fulfill_order(iterations);
    vector<intrank_t> &wait_order = fulfill_order;
    for (int i = 0; i < iterations; i++) {
      fulfill_order[i] = i;
      wait_order[i] = i;
    }
    std::shuffle(fulfill_order.begin(), fulfill_order.end(), g);
    barrier();
    // initiate all
    future<> all_fut = make_future();
    for (int i = 0; i < iterations; i++) {
      ASSERT(!pbs[fulfill_order[i]].get_future().is_ready());
      pbs[fulfill_order[i]].fulfill();
      auto fut = pbs[wait_order[i]].get_future();
      all_fut = when_all(all_fut, fut);
    }
    all_fut.wait();
    barrier();
  }

  {
    DBG("fulfill all then wait all different order\n");
    int iterations = 1000;
    vector<PromiseBarrier> pbs(iterations);
    vector<intrank_t> fulfill_order(iterations);
    vector<intrank_t> wait_order(iterations);
    for (int i = 0; i < iterations; i++) {
      fulfill_order[i] = i;
      wait_order[i] = i;
    }
    std::shuffle(fulfill_order.begin(), fulfill_order.end(), g);
    std::shuffle(wait_order.begin(), wait_order.end(), g);
    barrier();
    // initiate all
    future<> all_fut = make_future();
    for (int i = 0; i < iterations; i++) {
      ASSERT(!pbs[fulfill_order[i]].get_future().is_ready());
      pbs[fulfill_order[i]].fulfill();
      auto fut = pbs[wait_order[i]].get_future();
      all_fut = when_all(all_fut, fut);
    }
    all_fut.wait();
    barrier();
  }

  upcxx_utils::close_dbg();
  barrier();
  return 0;
}  // test_promise_barrier

template <typename T>
int test_promise_broadcast(int argc, char **argv) {
  barrier();
  upcxx_utils::open_dbg("test_promise_broadcast");
  vector<T> data;

  upcxx::future<> fut_chain = make_future();
  for (int i = 0; i < 2; i++) {
    auto &tm = i == 0 ? upcxx::world() : upcxx::local_team();

    int root = 0;
    int last_sz = 1;
    for (int sz = 1; sz < 16384; sz += last_sz / 7 + 1) {
      last_sz = sz;
      T val = sz + (i + 100) * rank_n() * rank_n() + root;
      DBG("i=", i, " sz=", sz, " val=", val, " root=", root, "\n");
      data.clear();
      data.resize(sz, val);
      auto sh_results = make_shared<vector<T>>(sz, 0);
      ASSERT(sh_results->size() == data.size());
      DBG("made sh_results=", sh_results.get(), " i=", i, " sz=", sz, " val=", val, "\n");
      PromiseBroadcast<T> pb(root, tm);
      // capture sh_results and verify it after PB is fulfilled
      auto fut1_check = pb.get_future().then([sh_results, i, sz, val, root]() {
        DBG("pb broadcast completed. Checking assertions i=", i, " sz=", sz, " val=", val, " root=", root,
            " sh_results=", sh_results.get(), "\n");
        ASSERT(sh_results->size() == sz);
        for (auto &x : *sh_results) {
          // DBG("x=", x, "\n");
          ASSERT(x == val);
        }
        DBG("pb i=", i, " sz=", sz, " root=", root, " checks\n");
      });

      // set the values for the root copy
      ASSERT((*sh_results)[0] == 0);
      if (root == tm.rank_me()) *sh_results = data;
      ASSERT((*sh_results).size() == sz);
      DBG("pb fulfilling sh_results=", sh_results.get(), " i=", i, " sz=", sz, " val=", val, "\n");
      pb.fulfill(sh_results->data(), sz);

      // make a new sh_results and construct a new PB
      sh_results = make_shared<vector<T>>(sz, 0);
      DBG("pb2 sh_results=", sh_results.get(), " i=", i, " sz=", sz, " val=", val, "\n");
      auto sh_pb2 = make_shared<PromiseBroadcast<T>>(root, tm);
      auto &pb2 = *sh_pb2;
      DBG("Constructed pb2 i=", i, " sz=", sz, "\n");
      // capture sh_result and verify it after PB2 is fulfilled
      auto fut2_check = pb2.get_future().then([sh_results, i, sz, val, root, sh_pb2]() {
        DBG("pb2 broadcasst completed. Checking assertions i=", i, " sz=", sz, " root=", root, " sh_results=", sh_results.get(),
            "\n");
        ASSERT(sh_results->size() == sz);
        for (auto &x : *sh_results) {
          // DBG("2 x=", x, "\n");
          ASSERT(x == val);
        }
        DBG("pb2 i=", i, " sz=", sz, " root=", root, " checks\n");
      });

      ASSERT((*sh_results)[0] == 0);
      if (root == tm.rank_me()) *sh_results = data;
      DBG("Fulfilling2 after pb is complete. sh_results=", sh_results.get(), " sz=", sz, "\n");
      auto fut2_fulfill = pb.get_future().then([&pb2, sh_pb2, sh_results, sz, i]() {
        DBG("pb completed - fut2. Fulfilling pb2 with results i=", i, " sz=", sz, " sh_results=", sh_results.get(), "\n");
        pb2.fulfill(sh_results->data(), sz);
        DBG("fut2 fulfilled pb2 i=", i, " sz=", sz, "\n");
      });

      fut_chain = when_all(fut_chain, fut1_check, fut2_check, fut2_fulfill).then([sz, i]() {
        DBG("Both pb and pb2 completed and checked i=", i, " sz=", sz, "\n");
      });
      progress();

      root = (root + 1) % tm.rank_n();
    }
    PromiseBroadcast<T> pb_never_fulfilled(root, tm);
    progress();
  }
  fut_chain.wait();

  upcxx_utils::close_dbg();
  barrier();
  return 0;
}  // test_promise_broadcast

int test_promise_reduce(int argc, char **argv) {
  barrier();
  upcxx_utils::open_dbg("test_promise_reduce");

  {
    double d1 = 2.0, d2 = 3.14159265358979;
    int64_t tmp = op_PromiseReduce::double2T(d1);
    ASSERT_FLOAT(op_PromiseReduce::T2double(tmp), d1, "");
    tmp = op_PromiseReduce::double2T(d2);
    ASSERT_FLOAT(op_PromiseReduce::T2double(tmp), d2, "");
  }

  {
    PromiseReduce pr /*mixed*/, pr_2 /*only reduce_all*/, pr_3 /*only reduce_one*/;
    auto fut_min = pr.reduce_one(rank_me(), op_fast_min);
    auto fut_a_min = pr_2.reduce_all(rank_me(), op_fast_min);
    auto fut_max = pr.reduce_one(rank_me(), op_fast_max);
    auto fut_a_max = pr.reduce_all(rank_me(), op_fast_max);
    auto fut_sum = pr.reduce_one(rank_me(), op_fast_add);
    auto fut_a_sum = pr.reduce_all(rank_me(), op_fast_add);

    auto fut_min_d = pr.reduce_one(rank_me() * 1.0 / rank_n(), op_fast_min);
    auto fut_a_min_d = pr.reduce_all(rank_me() * 1.0 / rank_n(), op_fast_min);
    auto fut_max_d = pr.reduce_one(rank_me() * 1.0 / rank_n(), op_fast_max);
    auto fut_a_max_d = pr.reduce_all(rank_me() * 1.0 / rank_n(), op_fast_max);
    auto fut_sum_d = pr.reduce_one(rank_me() * 1.0 / rank_n(), op_fast_add);
    auto fut_a_sum_d = pr_2.reduce_all(rank_me() * 1.0 / rank_n(), op_fast_add);
    auto fut_max_d2 = pr.reduce_one(rank_me() * 1.0 / rank_n(), op_fast_max);

    auto fut_min1 = pr_3.reduce_one(rank_me(), op_fast_min);
    auto fut_max1 = pr_3.reduce_one(rank_me(), op_fast_max);
    auto fut_sum1 = pr_3.reduce_one(rank_me(), op_fast_add);

    auto sum = 0;
    for (int i = 0; i < rank_n(); i++) sum += i;
    if (!rank_me()) INFO("sum = ", sum, "\n");

    pr_3.fulfill().wait();
    if (rank_me() == 0) {
      ASSERT(0 == fut_min1.wait());
      ASSERT(rank_n() - 1 == fut_max1.wait());
      ASSERT(sum == fut_sum1.wait());
    }

    pr.fulfill().wait();

    if (rank_me() == 0) {
      // DBG("fut_min=", fut_min.wait(), " fut_max=", fut_max.wait(), "\n");
      ASSERT(fut_min.wait() == 0 && "min in 0");
      ASSERT(fut_max.wait() == rank_n() - 1 && "max is last rank");
      // DBG("sum=", sum, " fut_sum=", fut_sum.wait(), "\n");
      ASSERT(fut_sum.wait() == sum && "sum is correct");
      // DBG("fut_min_d=", fut_min_d.wait(), " fut_max_d=", fut_max_d.wait(), " fut_sum_d=", fut_sum_d.wait(), "\n");
      ASSERT_FLOAT(fut_min_d.wait(), 0.0, "min is 0.0");
      ASSERT_FLOAT(fut_max_d.wait(), (rank_n() - 1) * 1.0 / (double)rank_n(), "max is last rank");
      ASSERT_FLOAT(fut_sum_d.wait(), sum / (double)rank_n(), "sum is correct");
      ASSERT_FLOAT(fut_max_d2.wait(), (rank_n() - 1) * 1.0 / (double)rank_n(), "max is last rank");
    }
    barrier();
    auto fut_all_results = pr_2.fulfill();
    fut_all_results.wait();
    INFO("fut_a_min=", fut_a_min.wait(), " fut_a_max=", fut_a_max.wait(), "\n");
    ASSERT(fut_a_min.wait() == 0 && "min in 0");
    ASSERT(fut_a_max.wait() == rank_n() - 1 && "max is last rank");
    // INFO("sum=", sum, " fut_sum=", fut_sum.wait(), "\n");
    ASSERT(fut_a_sum.wait() == sum && "sum is correct");
    // INFO("fut_min_d=", fut_a_min_d.wait(), " fut_max_d=", fut_a_max_d.wait(), " fut_sum_d=", fut_a_sum_d.wait(), "\n");
    ASSERT_FLOAT(fut_a_min_d.wait(), 0.0, "min in 0.0");
    ASSERT_FLOAT(fut_a_max_d.wait(), (rank_n() - 1.0) / (double)rank_n(), "max is last rank");
    ASSERT_FLOAT(fut_a_sum_d.wait(), sum / (double)rank_n(), "sum is correct");

    auto lme = local_team().rank_me();
    auto ln = local_team().rank_n();
    auto my_node = rank_me() / ln;
    INFO("lme=", lme, " ln=", ln, " my_node=", my_node, "\n");
    PromiseReduce pr3(local_team());
    auto tgt_root = ln / 2;
    auto fut_r = pr3.reduce_one(rank_me(), op_fast_max, tgt_root);
    auto fut_msm = pr3.msm_reduce_one(rank_me());
    auto fut_msm2 = pr3.msm_reduce_one(rank_me(), rank_me() != tgt_root, tgt_root);  // all active but 1
    auto fut_pr3 = pr3.fulfill();
    if (lme == 0) {
      auto msm = fut_msm.wait();
      ASSERT(msm.my == rank_me());
      ASSERT(msm.min == rank_me());
      ASSERT(msm.max == rank_me() + ln - 1);
      ASSERT(msm.active_ranks == ln);
    }
    if (lme == tgt_root) {
      auto msm = fut_msm2.wait();
      DBG(msm.to_string(), "\n");
      ASSERT(msm.my == rank_me());
      ASSERT(msm.min == rank_me() - lme);
      ASSERT(msm.max == rank_me() - lme + ln - 1);
      // ASSERT(msm.active_ranks == ln - 1 && "All active but 1");
    }

    PromiseReduce pr4(local_team());
    auto fut_pr4_1 = pr4.reduce_one(rank_me(), op_fast_max, tgt_root);
    auto fut_pr4_2 = pr4.reduce_all(rank_me(), op_fast_max);
    upcxx::promise<int> prom_me(1);
    auto fut_pr4_3 = pr4.fut_reduce_all(prom_me.get_future(), op_fast_max);
    prom_me.fulfill_result(lme);

    PromiseReduce pr5;
    std::vector<upcxx::future<int>> futs;
    for (int i = 0; i < 20000; i++) {
      futs.push_back(pr5.reduce_all(rank_me() + i, op_fast_max));
    }
    pr5.fulfill().wait();

    auto fut_pr4 = pr4.fulfill();

    fut_pr3.wait();
    if (lme == tgt_root) {
      auto r = fut_r.wait();
      INFO("got=", r, " exp=", (my_node + 1) * ln - 1, "\n");
      ASSERT(r == (my_node + 1) * ln - 1 && "world rank of last rank in my local team");
    }

    fut_pr4.wait();
    auto pr4_1 = fut_pr4_1.wait();
    auto pr4_2 = fut_pr4_2.wait();
    auto pr4_3 = fut_pr4_3.wait();
    if (lme == tgt_root) ASSERT(pr4_1 == (my_node + 1) * ln - 1 && "target root got world rank of last rank in my local team");
    ASSERT(pr4_2 == (my_node + 1) * ln - 1 && "all ranks got world rank of last rank in my local team");
    ASSERT(pr4_3 == ln - 1 && "all ranks got last rank");

    int i = 0;
    for (auto &fut : futs) {
      auto x = fut.wait();
      auto t = i++ + rank_n() - 1;
      ASSERT(t == x);
    }
  }

  upcxx_utils::close_dbg();
  barrier();
  return 0;
}  // test_promise_reduce

int test_fut_promise_reduce(int argc, char **argv) {
  barrier();
  upcxx_utils::open_dbg("test_promise_reduce");

  {
    PromiseReduce pr;

    auto fut_min = pr.reduce_one(rank_me(), op_fast_min);
    auto fut_max = pr.reduce_one(rank_me(), op_fast_max);
    auto fut_dmax = pr.reduce_one((double)rank_me() * 3.14, op_fast_max);
    promise<int> p_val;
    promise<bool> p_active;
    auto fut_msm = pr.fut_msm_reduce_one(p_val.get_future(), p_active.get_future());
    auto fut_msm2 = pr.fut_msm_reduce_one(p_val.get_future(), p_active.get_future());
    auto fut_msm3 = pr.fut_msm_reduce_one(p_val.get_future(), p_active.get_future());
    auto fut_msm4 = pr.fut_msm_reduce_one(p_val.get_future(), p_active.get_future());
    auto fut_msm5 = pr.fut_msm_reduce_one(p_val.get_future(), p_active.get_future());
    auto fut_msm6 = pr.fut_msm_reduce_one(p_val.get_future(), p_active.get_future());
    auto fut_msm7 = pr.fut_msm_reduce_one(p_val.get_future(), p_active.get_future());
    auto fut_msm8 = pr.fut_msm_reduce_one(p_val.get_future(), p_active.get_future());
    auto fut_msm9 = pr.fut_msm_reduce_one(p_val.get_future(), p_active.get_future());

    ASSERT(!fut_msm.is_ready());
    ASSERT(!fut_msm2.is_ready());
    ASSERT(!fut_msm9.is_ready());

    // cannot fulfill until all values are ready! auto fut = pr.fulfill();
    // ASSERT(!fut.is_ready());

    ASSERT(!fut_msm.is_ready());
    ASSERT(!fut_msm2.is_ready());
    ASSERT(!fut_msm9.is_ready());

    p_val.fulfill_result(rank_me());
    p_active.fulfill_result(true);

    auto fut = pr.fulfill();
    fut.wait();

    ASSERT(fut.is_ready());
    auto msm1 = fut_msm.wait();
    auto msm2 = fut_msm2.wait();
    auto msm3 = fut_msm3.wait();
    auto msm9 = fut_msm9.wait();
    LOG("1: ", msm1.to_string(), "\n");
    LOG("2: ", msm2.to_string(), "\n");
    LOG("3: ", msm3.to_string(), "\n");
    LOG("9: ", msm9.to_string(), "\n");
    if (rank_me() == 0) {
      ASSERT(msm1.min == 0);
      ASSERT(msm1.max == rank_n() - 1);
      ASSERT(msm2.max == rank_n() - 1);
      ASSERT(msm9.max == rank_n() - 1);
      ASSERT(fut_dmax.wait() == (rank_n() - 1) * 3.14);
    }
  }

  upcxx_utils::close_dbg();
  barrier();
  return 0;
}

int test_promise_collectives(int argc, char **argv) {
  test_promise_barrier(argc, argv);
  test_promise_broadcast<int16_t>(argc, argv);
  test_promise_broadcast<uint32_t>(argc, argv);
  test_promise_broadcast<uint64_t>(argc, argv);
  test_promise_broadcast<float>(argc, argv);
  test_promise_broadcast<double>(argc, argv);
  test_promise_reduce(argc, argv);
  test_fut_promise_reduce(argc, argv);
  return 0;
}
