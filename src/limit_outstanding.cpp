#include <cassert>
#include <upcxx/upcxx.hpp>
#include <upcxx_utils/limit_outstanding.hpp>
#include <upcxx_utils/log.hpp>

using upcxx::when_all;
using LimitedFutureQueue = std::deque<upcxx::future<> >;

LimitedFutureQueue &upcxx_utils::_get_outstanding_queue() {
  static LimitedFutureQueue outstanding_queue;
  return outstanding_queue;
}

int &upcxx_utils::default_limit_outstanding_count() {
  static int _ = -1;
  return _;
}

void upcxx_utils::collapse_outstanding_futures(int limit, LimitedFutureQueue &outstanding_queue) {
  // performs progress until the futures within outstanding_queue become ready and can be shrunk to limit
  assert(!upcxx::in_progress());
  size_t nonce = 0;
  if (limit < 0) limit = default_limit_outstanding_count() >= 0 ? default_limit_outstanding_count() : upcxx::local_team().rank_n();
  while (outstanding_queue.size() > limit) {
    while (outstanding_queue.size() > 0 && outstanding_queue.front().is_ready()) outstanding_queue.pop_front();
    while (outstanding_queue.size() > 1 && outstanding_queue.back().is_ready()) outstanding_queue.pop_back();
    if (outstanding_queue.size() <= limit) break;
    assert(outstanding_queue.size() > 0);
    auto idx = nonce++ % outstanding_queue.size();
    if (outstanding_queue[idx].is_ready()) {
      if (idx != outstanding_queue.size() - 1) std::swap(outstanding_queue[idx], outstanding_queue.back());
      outstanding_queue.pop_back();
    } else {
      upcxx::progress();
    }
  }
}

upcxx::future<> upcxx_utils::collapse_outstanding_futures_async(int limit, LimitedFutureQueue &outstanding_queue, int max_check) {
  // pop any ready futures at the front of the queue presuming that FIFO ordering is dominant
  // then combined/collapse futures when limit is exceeded
  // returning a future that needs to be waited eventually
  if (limit < 0) limit = default_limit_outstanding_count() >= 0 ? default_limit_outstanding_count() : upcxx::local_team().rank_n();
  while (outstanding_queue.size() > 0 && outstanding_queue.front().is_ready()) outstanding_queue.pop_front();
  while (outstanding_queue.size() > 1 && outstanding_queue.back().is_ready()) outstanding_queue.pop_back();
  upcxx::future<> returned_future = upcxx::make_future();
  if (outstanding_queue.size() >= limit) {
    // reduce to limit when over
    // collapsing first queued futures into the returned future, presuming that FIFO ordering is dominant
    while (outstanding_queue.size() > limit) {
      auto fut = outstanding_queue.front();
      outstanding_queue.pop_front();
      if (!fut.is_ready()) returned_future = upcxx::when_all(fut, returned_future);
    }
    // DBG("limit=", limit, " outstanding=", outstanding_queue.size(), " max_check=", max_check, "\n");
    if (limit == 0) {
      assert(outstanding_queue.empty());
    } else {
      assert(outstanding_queue.size() <= limit);
      auto sz = outstanding_queue.size();
      if (max_check >= 3 * sz / 4) {
        if (max_check > sz) max_check = sz;
      }
      static size_t nonce = 0;  // used to iterate and wrap through the queue between calls
      for (int i = 0; i < max_check; i++) {
        int idx = ++nonce % sz;
        auto &test_fut = outstanding_queue[idx];
        if (test_fut.is_ready()) {
          std::swap(test_fut, returned_future);
          assert(returned_future.is_ready());
          break;
        }
      }
    }
  }
  // DBG("limit=", limit, " outstanding=", outstanding_queue.size(), " max_check=", max_check, ", ret=", returned_future.is_ready(),
  // "\n");
  return returned_future;
}

void upcxx_utils::add_outstanding_future(upcxx::future<> fut, LimitedFutureQueue &outstanding_queue) {
  if (!fut.is_ready()) outstanding_queue.push_back(fut);
}

upcxx::future<> upcxx_utils::limit_outstanding_futures(int limit, LimitedFutureQueue &outstanding_queue) {
  assert(!upcxx::in_progress());
  collapse_outstanding_futures(limit, outstanding_queue);
  return upcxx::make_future();
}

upcxx::future<> upcxx_utils::limit_outstanding_futures(upcxx::future<> fut, int limit, LimitedFutureQueue &outstanding_queue) {
  assert(!upcxx::in_progress());
  if (limit < 0) limit = default_limit_outstanding_count() >= 0 ? default_limit_outstanding_count() : upcxx::local_team().rank_n();
  // DBG("limit=", limit, " outstanding=", outstanding_queue.size(), "\n");
  add_outstanding_future(fut, outstanding_queue);
  return limit_outstanding_futures(limit, outstanding_queue);
}

upcxx::future<> upcxx_utils::flush_outstanding_futures_async(LimitedFutureQueue &outstanding_queue) {
  // DBG_VERBOSE(" outstanding=", outstanding_queue.size(), "\n");
  upcxx::future<> all_fut = collapse_outstanding_futures_async(0, outstanding_queue);
  return all_fut;
}

void upcxx_utils::flush_outstanding_futures(LimitedFutureQueue &outstanding_queue) {
  assert(!upcxx::in_progress());
  while (!outstanding_queue.empty()) {
    auto fut = flush_outstanding_futures_async(outstanding_queue);
    wait_wrapper(fut);
  }
  assert(outstanding_queue.empty());
}
